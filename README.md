# bluematter
Very small prohject with python code for a Raspberry pi3 to scan for bluetooth devices and inform mattermost of known people.

Contains some scripts:

 - bluematter.py:
 main script, polls for bluetooth devices.
 Checks if a people table already exists, otherwise creates db and table
 If device is recognised checks if this is the first time today
 If yes, could send message to mattermost or other application
 - blueinsert.py:
 script to insert name into people table.
 Uses 2 arguments: device address and name
 - bluedump.py:
 Checks if table people exists.
 If yes, do select * and display results.
 - dbinfo.py: Script from the Internets to show some db statistics.
