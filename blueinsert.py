#!/usr/bin/python
# file: inquiry.py
# auth: Albert Huang <albert@csail.mit.edu>
# desc: performs a simple device inquiry followed by a remote name request of
#       each discovered device
# $Id: inquiry.py 401 2006-05-05 19:07:48Z albert $
#

import bluetooth
import sqlite3
import sys

print("Setup database connection")
conn = sqlite3.connect('bluematter.db')
print("Opened database successfully");

print("Check if table 'people' exists.")
cursor=conn.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='people';")
tableFound=False
for row in cursor:
  if row[0] == 'people':
    tableFound=True
    print("Table 'people' already exists. Good.")
    break

#
if tableFound == False:
  print("Table 'people' was not found. Exiting.")
  sys.exit(1)

#print str(sys.argv)
print("Device     : " + sys.argv[1] + " (" + str(len(sys.argv[1])) + ")")
print("Device name: " + sys.argv[2])

if len(sys.argv[1]) == 17:
  #Good length
  conn.execute("INSERT INTO people (addr,ourname) VALUES (?,?)" , ( str(sys.argv[1]), str(sys.argv[2]) ) )
  conn.commit()

else:
  print("Expecting device addr string length 17. Quitting")
  sys.exit(1)

conn.close()
