#!/usr/bin/python
# file: inquiry.py
# auth: J. Baten <jbaten@i2rs.nl>, based on a program written by Albert Huang <albert@csail.mit.edu>
# desc: performs a simple device inquiry followed by a remote name request of each discovered device
#       and stores it in a local sqlite database
# $Id$
#

import bluetooth
import requests
import sqlite3
import datetime

print("Setup database connection")
conn = sqlite3.connect('bluematter.db')
print("Opened database successfully");

print("Check if table 'people' exists.")
cursor=conn.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='people';")
tableFound=False
for row in cursor:
  if row[0] == 'people':
    tableFound=True
    print("Table 'people' already exists. Good.")
    break

#
if tableFound == False:
  print("Table 'people' was not found. Creating now.")
  conn.execute('''create table people
       (id integer primary key autoincrement    not null,
       addr           char(32) not null,
       devname        text    ,
       ourname        char(50) not null,
       lastseen       integer);
  ''')
  conn.execute('''create index addr_idx on people(addr);''')
  print("Table 'people' created successfully");



print("Entering loop...")
while True:
  print("performing inquiry...")

  nearby_devices = bluetooth.discover_devices(lookup_names = True)

  print("found %d devices" % len(nearby_devices))

  for addr, name in nearby_devices:
    print("  %s - %s" % (addr, name))
    #Is this someone we know? Lookup in people table
    cursor=conn.execute("select id,addr,devname,ourname,date(lastseen) from people where addr = ? ",(addr,) )
    data=cursor.fetchone()
    if data is None:
       #Device unknown. Ignoring
       print("Unknown person. Ignoring")
    else:
       #We know this person
       print("Known person =>")
       print("  id          : ", data[0])
       print("  address     : ", data[1])
       print("  device name : ", data[2])
       print("  our name    : ", data[3])
       print("  last seen   : ", data[4])
  
       #Did we see this person before today? Check field latest ping
       today=datetime.date.today()
       if data[4] is None:
         print("No date on record. Updating.")
         cursor=conn.execute("update people set lastseen = date('now'), devname = ? where id = ?", (name,data[0],) )
         conn.commit()
       else:
         if str(data[4]) == str(today):
           print("Already seen today")
         else:
           print("First seen today. Informing mattermost.")
           print("Updating lastseen field.")
           cursor=conn.execute("update people set lastseen = date('now') where id = ?", (data[0],) )
           conn.commit()
    
conn.close()
